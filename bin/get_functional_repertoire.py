#!/usr/bin/env python3
import sys
from bblsuite import globals
globals.QUIET = True
from bblsuite.network.mysql import MySQLHandler
import argparse
import operator

def main():
    
    
    #### commandline argument reading
    ### idk if you want to do that differently
    argp = argparse.ArgumentParser()
    argp.add_argument('-q', dest='QUERYID')
    argp.add_argument('-s', dest='SELECTEDIDS', required=True)
    args = argp.parse_args()

    uids = args.SELECTEDIDS.split(',')

    fusiondb = MySQLHandler('fusiondb')
    fdbmapper = MySQLHandler('jobsdb')

    ### main method logic
    clusters = {}
    if args.QUERYID is not None:
        clusters[args.QUERYID] = get_query_functionome(fdbmapper, args.QUERYID)
    for uid in uids:
        clusters[uid] = get_uid_functionome(fusiondb, uid)
    orgs_per_cluster, cluster_ids = generate_orgs_per_cluster(clusters)
    cluster_anno = generate_annotation_mapping(fusiondb, cluster_ids)
    for cluster in orgs_per_cluster:
        if cluster[0] == '':
            continue
        print(
            *(
                f"\"{cluster[0]}\"", 
                f"\"{len(cluster[1])}\"", 
                f"\"{cluster_anno[cluster[0]]}\""
            ), 
            sep='\t', 
            end='\n')


def generate_orgs_per_cluster(clusters):
    #### organisms per cluster counting logic
    orgs_per_cluster = {}
    for id_ in clusters:
        clusters_in_org = clusters[id_]
        for cluster in clusters_in_org:
            if cluster not in orgs_per_cluster:
                orgs_per_cluster[cluster] = set([id_])
            else:
                orgs_per_cluster[cluster].add(id_)
    sorted_orgs = sorted(orgs_per_cluster.items(), key=lambda x: len(x[1]), reverse=True)
    return sorted_orgs, orgs_per_cluster.keys()

def generate_annotation_mapping(fusiondb, clusters):
    cluster_string = "('{}')".format("','".join(clusters))
    # print(cluster_string)
    fusiondb.connect()
    sql = ("SELECT cluster_id, annotation FROM cluster_annotation WHERE cluster_id IN {}".format(cluster_string))
    result_set = fusiondb.query(sql)
    fusiondb.disconnect()  
    cluster_anno = {}
    for entry in result_set:
        cluster_anno[entry[0]] = entry[1].split(';')[0][1:].replace('"', '')
    return cluster_anno


def get_query_functionome(fdbmapper, queryid):
    ### habe jetzt fuer test zzwecke einfach kein peewee ding verwendet
    ### wenn du das db interface verwenden willst das du nutzt hier die query noch mal
    # SELECT
    #     cluids
    # FROM
    #     annotation_results
    # WHERE
    #     jobid = JOBID

    fdbmapper.connect()
    sql = ("SELECT cluster_ids FROM annotation_results WHERE jobid='%s'" % queryid)
    id_list = fdbmapper.query(sql)[0][0]
    fdbmapper.disconnect()
    return set(id_list.split(';'))

def get_uid_functionome(fusiondb, uid):
    ### same here:
    # SELECT DISTINCT
    #    (cluster_id)
    # FROM
    #    protein_map
    #        JOIN
    #    protein_to_functional_cluster ON protein_map.gene_id = protein_to_functional_cluster.gene_id
    # WHERE
    #     protein_map.organism_id = UID

    ids = set()
    fusiondb.connect()
    sql = "SELECT DISTINCT(cluster_id) FROM protein_map JOIN protein_to_functional_cluster" \
          " ON protein_map.gene_id = protein_to_functional_cluster.gene_id WHERE protein_map.organism_id='%s'" % uid
    result_set = fusiondb.query(sql)
    fusiondb.disconnect()
    for entry in result_set:
        ids.add(entry[0])
    return ids

if __name__ == '__main__':    
    try:
        main()
    except KeyboardInterrupt:
        pass

