"""FusionDB2 Network creation"""

import xml.etree.ElementTree as ET
import sys


class FusionNetwork:
    def __init__(self, job_id, network_type=None, scope=None, request_ids=None, query=None):
        self.network_type = network_type
        self.job_id = job_id
        self.scope = scope
        self.request_ids = request_ids
        self.network = None
        self.meta = {}
        self.query = query
        if network_type == 'fusionPlus':
            self.query_size = 20
            self.organism_size = 10
            self.clusternodes_size = 1
        elif network_type == 'fusion':
            self.query_size = 20
            self.organism_size = 10
            self.clusternodes_size = 1
        else:
            self.query_size = 20
            self.organism_size = 10
            self.clusternodes_size = 1

    def create_fusion(self):
        """Creates a fusion network based on fusiondb entries"""

        job_db = MySQLHandler('jobsdb')
        fusion_db = MySQLHandler('fusiondb')

        vertices = {}
        edges = []

        if self.query:
            """
            If the request comes from mapping then query will be set to
            true -> only then a connection to the jobsdb will be
            established and similarity to other organisms in fusiondb
            will be extracted
            """
            vertices[self.job_id] = "organism"

            job_db.connect()
            similarity_result_query = "SELECT organism_id, similarity FROM similarity_results WHERE jobid='%s'" % self.job_id
            similarity_result = job_db.query(similarity_result_query)
            job_db.disconnect()

            for similarity in similarity_result:
                similarity_val = similarity[1]
                if similarity_val > 0:
                    if similarity[0] in self.request_ids:
                        organism_id = similarity[0]
                        vertices[organism_id] = "organism"
                        edges.append({'v1': self.job_id, 'v2': organism_id, 'weight': similarity_val})
                    elif self.scope == 'full':
                        organism_id = similarity[0]
                        vertices[organism_id] = "organism"
                        edges.append({'v1': self.job_id, 'v2': organism_id, 'weight': similarity_val})

        """
        starting to gather all (or a subset) of the organisms
        simialrities from fusiondb
        """
        fusion_db.connect()
        request_ids_str = "','".join(self.request_ids)
        if self.scope == "full":
            limited = ""
        else:
            limited = "WHERE organism_id_1 IN ('%s') AND organism_id_2 IN ('%s')" % (request_ids_str, request_ids_str)
        similarities_query = "SELECT organism_id_1, organism_id_2, similarity FROM organism_to_organism_similarity %s" % limited #  WHERE organism_id_1 IN ('%s') AND organism_id_2 IN ('%s')" % (request_ids_str, request_ids_str)
        similarities = fusion_db.query(similarities_query)
        metadata_query = "SELECT organisms.organism_id, organisms.name, organisms.ncbi_taxid, organisms.species, organisms.genus, organisms.family, organisms.order, organisms.class, organisms.phylum, metadata.salinity, metadata.ph_value, metadata.temperature, metadata.oxygen_requirement, metadata.habitat FROM metadata JOIN organisms ON organisms.organism_id = metadata.organism_id"  # WHERE organisms.organism_id IN ('%s')" % request_ids_str
        metadata = fusion_db.query(metadata_query)
        fusion_db.disconnect()

        organisms_meta = {}
        for meta in metadata:
            organisms_meta[meta[0]] = {'name': meta[1],
                                       'taxid': meta[2],
                                       'species': meta[3],
                                       'genus': meta[4],
                                       'family': meta[5],
                                       'order': meta[6],
                                       'class': meta[7],
                                       'phylum': meta[8],
                                       'salinity': meta[9],
                                       'ph_value': meta[10],
                                       'temperature': meta[11],
                                       'oxygen_requirement': meta[12],
                                       'habitat': meta[13],
                                       'size': '1'}
        self.meta['organisms'] = organisms_meta

        for similarity in similarities:
            similarity_val = similarity[2]
            if similarity_val > 0:
                organism_id_1 = similarity[0]
                organism_id_2 = similarity[1]
                vertices[organism_id_1] = "organism"
                vertices[organism_id_2] = "organism"
                edges.append({'v1': organism_id_1, 'v2': organism_id_2, 'weight': similarity_val})

        self.network = {'vertices': vertices, 'edges': edges}

    def create_fusionplus(self):
        """Creates a fusion+ network based on fusiondb entries"""

        job_db = MySQLHandler('jobsdb')
        fusion_db = MySQLHandler('fusiondb')

        vertices = {}
        edges = []

        if self.query:
            vertices[self.job_id] = "organism"

            job_db.connect()
            annotation_result_query = "SELECT cluster_ids FROM annotation_results WHERE jobid='%s'" % self.job_id
            annotation_result = job_db.query(annotation_result_query)[0]
            job_db.disconnect()

            clusterids = annotation_result[0].split(';')
            for cluid in clusterids:
                if(cluid != ""):
                    vertices[cluid] = "function"
                    edges.append({'v1': self.job_id, 'v2': cluid, 'weight': 1})


        fusion_db.connect()
        request_ids_str = "','".join(self.request_ids)
        ret_vals_query = "SELECT DISTINCT protein_to_functional_cluster.cluster_id, protein_map.organism_id FROM protein_to_functional_cluster JOIN protein_map ON protein_map.gene_id = protein_to_functional_cluster.gene_id AND protein_map.organism_id IN ('%s')" % request_ids_str
        ret_vals = fusion_db.query(ret_vals_query)
        metadata_query = "SELECT organisms.organism_id, organisms.name, organisms.ncbi_taxid, organisms.species, organisms.genus, organisms.family, organisms.order, organisms.class, organisms.phylum, metadata.salinity, metadata.ph_value, metadata.temperature, metadata.oxygen_requirement, metadata.habitat FROM metadata JOIN organisms ON organisms.organism_id = metadata.organism_id"  # WHERE organisms.organism_id IN ('%s')" % request_ids_str
        metadata = fusion_db.query(metadata_query)
        fusion_db.disconnect()

        organisms_meta = {}
        for meta in metadata:
            organisms_meta[meta[0]] = {'name': meta[1],
                                       'taxid': meta[2],
                                       'species': meta[3],
                                       'genus': meta[4],
                                       'family': meta[5],
                                       'order': meta[6],
                                       'class': meta[7],
                                       'phylum': meta[8],
                                       'salinity': meta[9],
                                       'ph_value': meta[10],
                                       'temperature': meta[11],
                                       'oxygen_requirement': meta[12],
                                       'habitat': meta[13],
                                       'members_count': 'None',
                                       'has_anno': '1',
                                       'pfam_count': 'None',
                                       'size': '1'}
        self.meta['organisms'] = organisms_meta

        func_nodes = set()
        for ret_val in ret_vals:
            organism_id = ret_val[1]
            cluster_id = ret_val[0]
            vertices[organism_id] = "organism"
            vertices[cluster_id] = "function"
            func_nodes.add(ret_val[0])
            edges.append({'v1': organism_id, 'v2': cluster_id, 'weight': 1})

        self.network = {'vertices': vertices, 'edges': edges}

        func_nodes_str = "','".join(func_nodes)
        if self.query:
            func_nodes_str = "{}','{}".format(func_nodes_str, "','".join(clusterids))
        # print(func_nodes_str, file=sys.stderr)
        # print(func_nodes_str)
        fusion_db.connect()
        func_meta_query = "SELECT cluster_meta.cluster_id, cluster_meta.func_anno, cluster_meta.members_count from cluster_meta where cluster_meta.cluster_id IN ('%s')" % func_nodes_str
        func_metadata = fusion_db.query(func_meta_query)
        pfam_count_query = "SELECT cluster_to_pfam_annotation.cluster_id, COUNT(cluster_to_pfam_annotation.pfam_id) FROM cluster_to_pfam_annotation WHERE cluster_to_pfam_annotation.cluster_id IN ('%s') GROUP BY cluster_to_pfam_annotation.cluster_id" % func_nodes_str
        pfam_count_data = fusion_db.query(pfam_count_query)
        fusion_db.disconnect()

        pfam_counts = {}
        for pfam_count in pfam_count_data:
            pfam_counts[pfam_count[0]] = pfam_count[1]
        # print(pfam_counts)


        func_cluster_metadata = {}
        for meta in func_metadata:
            has_anno = '1'
            if meta[1].lower() == 'unknown':
                has_anno = '0'
            if meta[1].lower().startswith('hypothetical') and meta[1].lower().endswith('protein'):
                has_anno ='0'
            for tag in ['unknown protein', 'unknown function', 'hypothetical protein', ': hypothetical']:
                if tag in meta[1].lower():
                    has_anno = '0'
            if meta[0] in pfam_counts:
                pfam_count = pfam_counts[meta[0]]
            else:
                pfam_count = 0
            func_cluster_metadata[meta[0]] = { 'name': meta[1],
                                               'taxid': 'None',
                                               'species': 'None',
                                               'genus': 'None',
                                               'family': 'None',
                                               'order': 'None',
                                               'class': 'None',
                                               'phylum': 'None',
                                               'salinity': 'None',
                                               'ph_value': 'None',
                                               'temperature': 'None',
                                               'oxygen_requirement': 'None',
                                               'habitat': 'None',
                                               'members_count': meta[2],
                                               'has_anno': has_anno,
                                               'pfam_count': pfam_count,
                                               'size': '1'}
            if meta[1] == "":
                func_cluster_metadata[meta[0]]['name'] = 'No annotation'
                func_cluster_metadata[meta[0]]['has_anno'] = '0'
        # print(func_cluster_metadata)
        self.meta['functions'] = func_cluster_metadata


    def create_abc_file(self):
        vertices = self.network['vertices']
        edges = self.network['edges']
        meta_organisms = self.meta['organisms']
        edge_count = len(edges)
        counter = 1
        vertex_idx = {}
        vertices.pop(self.job_id)
        vertex_idx[self.job_id] = counter
        counter += 1
        for vertex in vertices:
            try:
                meta = meta_organisms[vertex]
            except KeyError:
                meta = no_meta
            vertex_idx[vertex] = counter
            counter += 1
        result = ''
        for idx, edge in enumerate(edges):
            result += "%s %s %f%s" % (vertex_idx[edge['v1']], vertex_idx[edge['v2']], edge['weight'], '\n' if idx < edge_count - 1 else '')
        return result

    def create_gexf_file(self):
        vertices = self.network['vertices']
        edges_data = self.network['edges']
        meta_organisms = self.meta['organisms']
        if self.network_type == 'fusionPlus':
            meta_functions = self.meta['functions']

        gexf = ET.Element('gexf')
        gexf.set('xmlns', "http://www.gexf.net/1.3")
        gexf.set('version', "1.3")
        gexf.set('xmlns:viz', "http://www.gexf.net/1.3/viz")
        gexf.set('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance")
        gexf.set('xsi:schemaLocation', "http://www.gexf.net/1.3 http://www.gexf.net/1.3/gexf.xsd")

        # meta = ET.SubElement(gexf, 'meta')
        # creator = ET.SubElement(meta, 'creator')
        # creator.text('bormberglab_suite/network/fusiondb')

        graph = ET.SubElement(gexf, 'graph')
        graph.set('mode', 'static')
        graph.set('defaultedgetype', 'undirected')

        attributes = ET.SubElement(graph, 'attributes')
        attributes.set('class', 'node')
        attributes.set('type', 'static')

        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', "name")
        attribute.set('title', "name")
        attribute.set('type', "string")

        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', "taxid")
        attribute.set('title', "taxid")
        attribute.set('type', "string")

        phyl_levels = ["species", "genus", "family", "order", "class", "phylum"]
        metadata_types = ["salinity", "ph_value", "temperature", "oxygen_requirement", "habitat"]
        for level in phyl_levels:
            attribute = ET.SubElement(attributes, 'attribute')
            attribute.set('id', level)
            attribute.set('title', level)
            attribute.set('type', "string")
        for type_ in metadata_types:
            attribute = ET.SubElement(attributes, 'attribute')
            attribute.set('id', type_)
            attribute.set('title', type_)
            attribute.set('type', "string")
        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', 'size')
        attribute.set('title', 'size')
        attribute.set('type', "float")

        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', 'members_count')
        attribute.set('title', 'members_count')
        attribute.set('type', "int")

        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', 'has_anno')
        attribute.set('title', 'has_anno')
        attribute.set('type', "int")

        attribute = ET.SubElement(attributes, 'attribute')
        attribute.set('id', 'pfam_count')
        attribute.set('title', 'pfam_count')
        attribute.set('type', "int")

        no_meta = {'name': 'None', 'taxid': 'None', 'species': 'None', 'genus': 'None',
                        'family': 'None', 'order': 'None', 'class': 'None', 'phylum': 'None',
                        'salinity': 'None', 'ph_value': 'None', 'temperature': 'None',
                        'oxygen_requirement': 'None', 'habitat': 'None', 'size': '1', 'members_count': 'None', 'has_anno': '0', 'pfam_count' : '0'}
        meta = no_meta
        query_label = 'query'

        nodes = ET.SubElement(graph, 'nodes')
        counter = 1
        vertex_idx = {}
        if self.query:
            vertices.pop(self.job_id)
            node = ET.SubElement(nodes, 'node')
            node.set('id', str(counter))
            node.set('label', query_label)
            vertex_idx[self.job_id] = counter
            counter += 1
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'name')
            attvalue.set('value', str(meta['name']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'taxid')
            attvalue.set('value', str(meta['taxid']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'species')
            attvalue.set('value', str(meta['species']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'genus')
            attvalue.set('value', str(meta['genus']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'family')
            attvalue.set('value', str(meta['family']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'order')
            attvalue.set('value', str(meta['order']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'class')
            attvalue.set('value', str(meta['class']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'phylum')
            attvalue.set('value', str(meta['phylum']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'salinity')
            attvalue.set('value', str(meta['salinity']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'ph_value')
            attvalue.set('value', str(meta['ph_value']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'temperature')
            attvalue.set('value', str(meta['temperature']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'oxygen_requirement')
            attvalue.set('value', str(meta['oxygen_requirement']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'habitat')
            attvalue.set('value', str(meta['habitat']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'members_count')
            attvalue.set('value', str(meta["members_count"]))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'has_anno')
            attvalue.set('value', "1")
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'pfam_count')
            attvalue.set('value', "0")
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'size')
            attvalue.set('value', str(self.query_size))

        for vertex in vertices:
            if vertex[0:2] != "C_":
                meta = meta_organisms[vertex]
                meta["size"] = self.organism_size
            elif vertex[0:2] == "C_":
                meta = meta_functions[vertex]
                meta["size"] = self.clusternodes_size
            node = ET.SubElement(nodes, 'node')
            node.set('id', str(counter))
            node.set('label', vertex)
            vertex_idx[vertex] = counter
            counter += 1
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'name')
            attvalue.set('value', str(meta['name']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'taxid')
            attvalue.set('value', str(meta['taxid']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'species')
            attvalue.set('value', str(meta['species']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'genus')
            attvalue.set('value', str(meta['genus']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'family')
            attvalue.set('value', str(meta['family']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'order')
            attvalue.set('value', str(meta['order']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'class')
            attvalue.set('value', str(meta['class']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'phylum')
            attvalue.set('value', str(meta['phylum']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'salinity')
            attvalue.set('value', str(meta['salinity']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'ph_value')
            attvalue.set('value', str(meta['ph_value']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'temperature')
            attvalue.set('value', str(meta['temperature']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'oxygen_requirement')
            attvalue.set('value', str(meta['oxygen_requirement']))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'habitat')
            attvalue.set('value', str(meta['habitat']))
            if self.network_type == 'fusionPlus':
                attvalue = ET.SubElement(node, 'attvalue')
                attvalue.set('for', 'members_count')
                attvalue.set('value', str(meta["members_count"]))
                attvalue = ET.SubElement(node, 'attvalue')
                attvalue.set('for', 'pfam_count')
                attvalue.set('value', str(meta["pfam_count"]))
                attvalue = ET.SubElement(node, 'attvalue')
                attvalue.set('for', 'has_anno')
                attvalue.set('value', str(meta["has_anno"]))
            attvalue = ET.SubElement(node, 'attvalue')
            attvalue.set('for', 'size')
            attvalue.set('value', str(meta["size"]))
            #viz_size = ET.SubElement(node, 'viz:size')
            #viz_size.set('value', str(4.0))

        edges = ET.SubElement(graph, 'edges')
        counter = 1
        for edge_data in edges_data:
            edge = ET.SubElement(edges, 'edge')
            edge.set('id', str(counter))
            edge.set('source', str(vertex_idx[edge_data['v1']]))
            edge.set('target', str(vertex_idx[edge_data['v2']]))
            edge.set('weight', str(edge_data['weight']))
            counter += 1

        xml_str = ET.tostring(gexf, 'utf-8')
        reparsed = xml.dom.minidom.parseString(xml_str)
        return reparsed.toprettyxml(indent="\t")

    def read_gexf_file(self, gexf_path):
        """reads gexf file and stores nodes and edges into FusionNetwork
        object

        provided a gexf file with the standart gexf structure, the
        function performs the following tasks in the listed order:
        - searches for the <graph> tag and stores contet in an
          ElementTree object for further processing
        - searches for the <nodes> tag in the <graph> ElementTree object
          and stores the contents in an ElementTree object
        - iterates over all children of the <nodes> tag and stores the
          retrieved data into a vertices dictonary with 'id' as key and
          'name', 'size', 'pos_x' & 'pos_y' as value. If viz parameters
          are not set in the gexf values for node size and position are
          set to 'None'
        - searches for the <edges> tag in the <graph> ElementTree object
          and stores the contents in and ElementTree object
        - iterates over all children in the <edges> tag and stores the
          retrieved data into a edges dictionary with 'id' as key and
          'source', 'target' & 'weight' as key. If no edge weight could
          be found as <edge> attribute then the weight is set to 1
        - the two dictionaries are stored into the FusionNetwork.network
          variable as {'vertices': vertices_data, 'edges' : edges_data}

        Arguments:
            gexf_path {string} -- path to the input file
        """

        vertices_data = {}
        edges_data = []
        itemspace = {'item': 'http://www.gexf.net/1.3',
                     'viz': 'http://www.gexf.net/1.3/viz'}
        gexf = ET.parse(gexf_path).getroot()
        graph = gexf.find('item:graph', itemspace)

        organisms_meta = {}
        functions_meta = {}

        nodes = graph.find('item:nodes', itemspace)
        for node in nodes:
            try:
                size_space = node.find('viz:size', itemspace)
                size = float(size_space.attrib['value'])
            except KeyError:
                size = None
            except ValueError:
                size = size_space.attrib['value']

            try:
                pos_space = node.find('viz:position', itemspace)
                pos_x = float(pos_space.attrib['x'])
                pos_y = float(pos_space.attrib['y'])
            except KeyError:
                pos_x = None
                pos_y = None
            except ValueError:
                pos_x = pos_space.attrib['x']
                pos_y = pos_space.attrib['y']
            try:
                color_space = node.find('viz:color', itemspace)
                r = color_space.attrib['r']
                g = color_space.attrib['g']
                b = color_space.attrib['b']
            except KeyError:
                r = 0
                g = 0
                b = 0

            try:
                nodeId = int(node.attrib['id'])
            except ValueError:
                nodeId = node.attrib['id']

            meta = {}
            attvalues = node.find('item:attvalues', itemspace)
            
            node_label = node.attrib['label']
            vertices_data[nodeId] = "organism"
            
            meta['label'] = node_label
            meta['size_ly'] = size
            meta['pos_x'] =  pos_x
            meta['pos_y'] =  pos_y
            meta['col_r'] =  r
            meta['col_g'] =  g
            meta['col_b'] =  b
            for attvalue in attvalues.findall('item:attvalue', itemspace):
                meta[attvalue.attrib['for']] = attvalue.attrib['value']
            # if nodeID.startswith('C_')
            organisms_meta[nodeId] = meta

        self.meta['organisms'] = organisms_meta

        edges = graph.find('item:edges', itemspace)
        for edge in edges:
            try:
                weight = float(edge.attrib['weight'])
            except KeyError:
                weight = 1

            try:
                sourceNode = int(edge.attrib['source'])
                targetNode = int(edge.attrib['target'])
            except KeyError:
                sourceNode = edge.attrib['source']
                targetNode = edge.attrib['target']

            edge_data = {'source': sourceNode,
                         'target': targetNode,
                         'weight': weight}
            edges_data.append(edge_data)

        self.network = {'vertices': vertices_data, 'edges': edges_data}

    def create_json_file(self):
        return json.dumps(self.network)

    def create_cyt_file(self):
        vertices = self.network['vertices']
        edges = self.network['edges']
        meta_organisms = self.meta['organisms']
        query_size = 3
        query_label ='query'
        cytoscape_network = []
        try:
            vertices.pop(self.job_id)
            meta = {'name': 'None', 'taxid': 'None', 'species': 'None', 'genus': 'None',
                'family': 'None', 'order': 'None', 'class': 'None', 'phylum': 'None',
                'salinity': 'None', 'ph_value': 'None', 'temperature': 'None',
                'oxygen_requirement': 'None', 'habitat': 'None', 'size': query_size}
            cytoscape_network.append({'data': {'id': query_label, 'name': 'query', 'size': meta['size'], #  'size': meta['size_ly'],
                                           'orgname': meta['name'],
                                           'taxid': meta['taxid'],
                                           'species': meta['species'],
                                           'genus': meta['genus'],
                                           'family': meta['family'],
                                           'order': meta['order'],
                                           'class': meta['class'],
                                           'phylum': meta['phylum'],
                                           'salinity': meta['salinity'],
                                           'ph_value': meta['ph_value'],
                                           'temperature': meta['temperature'],
                                           'oxygen_requirement': meta['oxygen_requirement'],
                                           'col_r': meta['col_r'],
                                           'col_g': meta['col_g'],
                                           'col_b': meta['col_b'],
                                           'habitat': meta['habitat']},
                                           'position': {'x': x, 'y': y}})
        except KeyError:
            no_jobid = True
        for idx, vertex in enumerate(vertices.keys()):
            meta = meta_organisms[vertex]
            try:
                label = meta['label']
            except KeyError:
                label = vertex
            try:
                # size = meta['size_ly']
                size = meta['size']
            except KeyError:
                size = 1
            try:
                x = float(meta['pos_x'])
                y = 0-float(meta['pos_y'])
            except KeyError:
                x = 0
                y = 0
            except ValueError:
                x = 1
                y = 1
            cytoscape_network.append({'data': {'id': vertex, 'name': label, 'size': size,
                                               'hexcolor': 'ffc000',
                                               'orgname': meta['name'],
                                               'taxid': meta['taxid'],
                                               'species': meta['species'],
                                               'genus': meta['genus'],
                                               'family': meta['family'],
                                               'order': meta['order'],
                                               'class': meta['class'],
                                               'phylum': meta['phylum'],
                                               'salinity': meta['salinity'],
                                               'ph_value': meta['ph_value'],
                                               'temperature': meta['temperature'],
                                               'oxygen_requirement': meta['oxygen_requirement'],
                                               'col_r': meta['col_r'],
                                               'col_g': meta['col_g'],
                                               'col_b': meta['col_b'],
                                               'habitat': meta['habitat']},
                                               'position': {'x': x, 'y': y}})
        for edge in edges:
            #OLD: cytoscape_network.append({'data': {'source': edge['v1'], 'target': edge['v2'], 'weight': edge['weight']}})
            cytoscape_network.append({'data': {'source': edge['source'], 'target': edge['target'], 'weight': edge['weight']}})
        return json.dumps(cytoscape_network)


def main(args):
    network_type = args.NETWORK_TYPE
    output_format = args.OUT_FORMAT
    job_id = args.JOBID
    scope = args.SCOPE
    if args.REQUEST_IDS is not None:
        request_ids = args.REQUEST_IDS.split(',')
    else:
        request_ids = None
    query = args.QUERY_ID

    fusion = FusionNetwork(job_id, network_type, scope, request_ids, query)
    if fusion.network_type == "fusion":
        fusion.create_fusion()
    elif fusion.network_type == "fusionPlus":
        fusion.create_fusionplus()
    else:
        pass

    if output_format == "getocy":
        infile = args.GEXF_INFILE
        fusion.read_gexf_file(infile)
        print(fusion.create_cyt_file())
    elif fusion.network_type is not None:
        if output_format == "abc":
            print(fusion.create_abc_file())
        elif output_format == "gephi":
            print(fusion.create_gexf_file())
        elif output_format == "cytoscape":
            print(fusion.create_cyt_file())
        elif output_format == "json":
            print(fusion.create_json_file())
        


if __name__ == "__main__":
    import sys
    import json
    import xml.etree.ElementTree as ET
    import xml.dom.minidom
    import argparse

    sys.path.append(".")
    from bblsuite import globals

    globals.QUIET = True
    from bblsuite.network.mysql import MySQLHandler

    argp = argparse.ArgumentParser()
    argp.add_argument('-t', '--type', dest='NETWORK_TYPE',
                      choices=['fusion', 'fusionPlus'])
    argp.add_argument('-o', '--out_format', dest='OUT_FORMAT',
                      choices=['abc', 'gephi', 'cytoscape', 'json', 'getocy'],
                      required=True)
    argp.add_argument('-s', '--scope', dest='SCOPE',
                      choices=['full', 'subnet'])
    argp.add_argument('-g', '--gexf_in', dest='GEXF_INFILE')
    argp.add_argument('-i', '--reques_ids', dest='REQUEST_IDS')
    argp.add_argument('-q', '--query', dest='QUERY_ID', action='store_true')
    argp.add_argument('-nq' '--no_query', dest='QUERY_ID', action='store_false')
    argp.add_argument('-j', '--jobid', dest='JOBID', required=True)
    args = argp.parse_args()

    main(args)
