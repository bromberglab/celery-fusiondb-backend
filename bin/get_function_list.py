#!/usr/bin/env python3
from bblsuite import globals
globals.QUIET = True
from bblsuite.network.mysql import MySQLHandler
import argparse
import operator

def main():
    
    #### commandline argument reading
    ### idk if you want to do that differently
    argp = argparse.ArgumentParser()
    argp.add_argument('-c', dest='CLUSTERIDS', required=True)
    args = argp.parse_args()

    cids = args.CLUSTERIDS.replace(',', "','")

    fusiondb = MySQLHandler('fusiondb')
    annotations = get_annotation(cids, fusiondb)
    for annotation in annotations:
        print(
            *(
                f"\"{annotation[0]}\"", 
                f"\"{1}\"", 
                f"\"{annotation[1]}\""
            ), 
            sep='\t', 
            end='\n')

    ### main method logic

def get_annotation(cluster_ids, fusiondb):

    cluster_annotation = set()
    fusiondb.connect()
    sql = "SELECT cluster_id, annotation FROM cluster_annotation WHERE cluster_id IN ('{}')".format(cluster_ids)
    result_set = fusiondb.query(sql)
    fusiondb.disconnect()
    for entry in result_set:
        annotation = entry[1].split(';')[0].replace('"', '')
        cluster_annotation.add((entry[0], annotation))
    return cluster_annotation

if __name__ == '__main__':
    import sys

    try:
        main()
    except KeyboardInterrupt:
        pass

