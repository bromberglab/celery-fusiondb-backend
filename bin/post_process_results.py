#!/usr/bin/env python3

import argparse
import sys
from Bio import SeqIO
from bblsuite import globals
globals.QUIET = True
from bblsuite.network.mysql import MySQLHandler

def main():

    

    argp = argparse.ArgumentParser()
    argp.add_argument('-j', '--jobid', dest='JOBID', required=True)
    argp.add_argument('-f', '--fasta_in', dest='FASTA_IN', required=True)
    argp.add_argument('-r', '--result_in', dest='RESULT_IN')
    argp.add_argument(
        '-o', '--outfile', dest='OUTFILE',
        type=argparse.FileType('w'), default=sys.stdout
    )
    args = argp.parse_args()

    with open(args.RESULT_IN, 'r') as infile:
        cluster_mapping, clusters = read(infile)
    if len(cluster_mapping) == 0:
        print('0', file=args.OUTFILE)
    else:
        print('1', file=args.OUTFILE)
        with open(args.FASTA_IN, 'r') as infile:
            submitted_ids = get_submitted_ids(infile)
        
        annotation_results = prepare_annotation_results(cluster_mapping, clusters, submitted_ids, args.JOBID)
        mapping_results = prepare_mapping_results(cluster_mapping, args.JOBID)
        similarity_results = prepare_simialrity_results(cluster_mapping, clusters, submitted_ids, args.JOBID)

        store_results(annotation_results, mapping_results, similarity_results)
    

def store_results(annotation, mapping, similarity):
    jobsdb = MySQLHandler('jobsdb')

    jobsdb.connect()
    jobsdb.insert_many(
        """INSERT INTO mapping_results (jobid, sequence_id, seguid, cluster_id, hfsp)
        VALUES (%s, %s, %s, %s, %s)""",
        mapping)
    jobsdb.disconnect()

    jobsdb.connect()
    jobsdb.insert_many(
        """INSERT INTO similarity_results (jobid, organism_id, similarity, shared_funcs)
        VALUES (%s, %s, %s, %s)""", similarity)
    jobsdb.disconnect()

    jobsdb.connect()
    jobsdb.insert("""
    INSERT INTO annotation_results (jobid, singleton_count, aborted_count, cluster_ids)
        VALUES (%s, %s, %s, %s)""", annotation)
    jobsdb.disconnect()

def read(infile):
    cluster_mapping = {}
    clusters = set()
    for line in infile:
        s_line = line.rstrip('\n').split('\t')
        id_ = s_line[0]
        cluster = s_line[1]
        hfsp = s_line[2]
        cluster_mapping[id_] = ['NULL', cluster, hfsp]
        clusters.add(cluster)
    return cluster_mapping, clusters

def get_submitted_ids(infile):
    seq_ids = set()
    for record in SeqIO.parse(infile, "fasta") :
        seq_ids.add(record.id)
    return seq_ids

def prepare_annotation_results(cluster_mapping, clusters, submitted_ids, jobid):
    mapped_ids = cluster_mapping.keys()
    singletons = submitted_ids.difference(mapped_ids)
    cluster_ids = clusters
    return (jobid, len(singletons), 0, ';'.join(cluster_ids))

def prepare_mapping_results(cluster_mapping, jobid):
    values = []
    for id_ in cluster_mapping:
        values.append((jobid, id_, cluster_mapping[id_][0], cluster_mapping[id_][1], cluster_mapping[id_][2]))
    return values

def prepare_simialrity_results(cluster_mapping, clusters, submitted_ids, jobid):
    values = []
    
    mapped_ids = cluster_mapping.keys()
    singletons = submitted_ids.difference(mapped_ids)
    cluster_ids = clusters

    fusion_db = MySQLHandler('fusiondb')
    fusion_db.connect()
    orgs = set()
    org_query = 'SELECT organisms.organism_id FROM organisms'
    ret = fusion_db.query(org_query)
    fusion_db.disconnect()
    for val in ret:
        orgs.add(val[0])
    
    for org in orgs:
        ref_cluster_ids = set()
        fusion_db.connect()
        cluster_query = "SELECT protein_to_functional_cluster.cluster_id FROM protein_to_functional_cluster JOIN protein_map ON protein_to_functional_cluster.gene_id = protein_map.gene_id WHERE protein_map.organism_id = ('%s')" % org
        ret = fusion_db.query(cluster_query)
        fusion_db.disconnect()
        for val in ret:
            ref_cluster_ids.add(val[0])
        
        if len(ref_cluster_ids) > (len(cluster_ids) + len(singletons)):
            sim = (
                len(ref_cluster_ids.intersection(cluster_ids)) /
                len(ref_cluster_ids))
        else:
            sim = (
                len(ref_cluster_ids.intersection(cluster_ids)) /
                (len(cluster_ids) + len(singletons)))
        shared = len(ref_cluster_ids.intersection(cluster_ids))
        
        values.append((jobid, org, sim, shared ))
    
    return values

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt: 
        pass
