#!/usr/bin/env python3
from bblsuite import globals
globals.QUIET = True
from bblsuite.network.mysql import MySQLHandler
import sys
import argparse
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC


def main():
    

    argp = argparse.ArgumentParser()
    argp.add_argument('-c', '--clusterid', dest='CLUSTERID', required=True)
    argp.add_argument('-o', '--outfile', dest='OUTFILE',
        type=argparse.FileType('w'), default=sys.stdout)
    args = argp.parse_args()
    fusiondb = MySQLHandler('fusiondb')
    # cluster_id = 'C_0'
    md5_hashes = get_md5_hashes(fusiondb, args.CLUSTERID)
    fusiondb.connect()
    for gene_id in md5_hashes:
        sequence = get_sequnce(fusiondb, md5_hashes[gene_id])
        record = SeqRecord(Seq(sequence, IUPAC.protein), id="gi:{}".format(gene_id), description="")
        SeqIO.write(record, args.OUTFILE, 'fasta')
    fusiondb.disconnect()


def get_md5_hashes(fusiondb, cluster_id):
    md5_hashes = {}
    fusiondb.connect()
    sql = "SELECT pm.gene_id, pm.md5_hash FROM protein_to_functional_cluster AS ptfc JOIN protein_map AS pm ON pm.gene_id = ptfc.gene_id WHERE ptfc.cluster_id = '{}'".format(cluster_id)
    resultset = fusiondb.query(sql)
    for result in resultset:
        md5_hashes[result[0]] = result[1]
    fusiondb.disconnect()
    return md5_hashes

def get_sequnce(fusiondb, md5_hash):
    # fusiondb.connect()
    sql = "SELECT sequence from protein_sequences WHERE md5_hash = '{}'".format(md5_hash)
    result = fusiondb.query(sql)
    # fusiondb.disconnect()
    return result[0][0]

if __name__ == '__main__':


    try:
        main()
    except KeyboardInterrupt:
        pass
