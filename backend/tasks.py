import os
import sys
import shutil
import urllib.request
import urllib.parse
from datetime import datetime
from pathlib import Path
from .helpers import run_command
from .worker import app


HOSTNAME = os.getenv('HOSTNAME', 'python-backend')
APP_PATH = Path(os.getenv('APP_PATH', '/app'))
APP_SCRATCH = Path(os.getenv('APP_SCRATCH', '/scratch'))
KEEP_APP_SCRATCH_DAYS = int(os.getenv('KEEP_APP_SCRATCH_DAYS', 7))
# TODO @ymahlich -> do not call python from command line here, simply import in python!
PYTHON = sys.executable


@app.task(bind=True, name='cron', queue='beat')
def cron(self, tasks):  
    for task in tasks:
        print(f'got task: {task}')
        if task == 'cleanup':
            for sfolder in APP_SCRATCH.glob('*'):
                createTime = datetime.fromtimestamp(sfolder.stat().st_atime)
                delta = datetime.now() - createTime
                if delta.days > KEEP_APP_SCRATCH_DAYS:
                    shutil.rmtree(sfolder)
        elif task == 'beat':
            print(f'Got beat: {HOSTNAME}')


@app.task(bind=True, name='python-backend.createGEXFNetwork.submit')  
def submit_create_network(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend network creation submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['jobid']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(parents=True, exist_ok=True)

    stdout = run_command(
        [
            PYTHON,
            'bin/fusiondb.py',
            '--type',
            kwargs['type'],
            '--out_format',
            kwargs['out_format'],
            '--scope',
            kwargs['scope'],
            '--reques_ids',
            kwargs['requested_ids'],
            '--jobid',
            kwargs['jobid'],
            kwargs['query_incl']
        ],
        print_output=False
    )

    outf_name = scratch_folder / kwargs['outfile']
    with open(outf_name, 'w') as outfile:
        print(*stdout, sep='\n', file=outfile)

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs['outfile']

@app.task(bind=True, name='python-backend.convertGEXFtoCytoscapeNetwork.submit')  
def submit_convert_gexf_to_cytoscape_network(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend network conversion submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['jobid']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(mode=0o777, parents=True, exist_ok=True)

    stdout = run_command(
        [
            PYTHON,
            'bin/fusiondb.py',
            '--jobid',
            kwargs['jobid'],
            '--gexf_in',
            scratch_folder / kwargs['infile'],
            '--out_format',
            kwargs['out_format']
        ],
        print_output=False
    )

    outf_name = scratch_folder / kwargs['outfile']
    with open(outf_name, 'w') as outfile:
        print(*stdout, sep='\n', file=outfile)

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs['outfile']

@app.task(bind=True, name='python-backend.mappingJob.submit')  
def submit_mapping_job(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend mapping job submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['job_id']

    scratch_folder = APP_SCRATCH / 'uploads'
    
    job_id = kwargs['job_id']
    infile_path = scratch_folder / kwargs['infile']

    print(''.join(['job_id: "', job_id, '"; infile_path: "', str(infile_path), '"']))

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs

@app.task(bind=True, name='python-backend.getFunctionalRepertoire')  
def submit_get_functional_repertoire(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend functional reperoire generation submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['_token']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(mode=0o777, parents=True, exist_ok=True)
    
    if kwargs['query_id'] == 'none':
        stdout = run_command(
            [
                PYTHON,
                'bin/get_functional_repertoire.py',
                '-s',
                kwargs['selected_ids']
            ],
            print_output=False
        )

    else:
        stdout = run_command(
            [
                PYTHON,
                'bin/get_functional_repertoire.py',
                '-s',
                kwargs['selected_ids'],
                '-q',
                kwargs['query_id']
            ],
            print_output=False
        )

    outf_name = scratch_folder / kwargs['outfile']
    with open(outf_name, 'w') as outfile:
        print(*stdout, sep='\n', end='', file=outfile)

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs['outfile']

@app.task(bind=True, name='python-backend.getFunctionList')  
def submit_get_function_list(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend function list submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['_token']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(mode=0o777, parents=True, exist_ok=True)
    
    stdout = run_command(
        [
            PYTHON,
            'bin/get_function_list.py',
            '-c',
            kwargs['cluster_ids']
        ],
        print_output=False
    )

    outf_name = scratch_folder / kwargs['outfile']
    with open(outf_name, 'w') as outfile:
        print(*stdout, sep='\n', end='', file=outfile)

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs['outfile']

@app.task(bind=True, name='python-backend.getFunctionFasta')  
def submit_get_function_fasta(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend function fasta file generation submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['_token']

    scratch_folder = APP_SCRATCH / kwargs['_token']
    os.umask(0)
    scratch_folder.mkdir(mode=0o777, parents=True, exist_ok=True)
    
    outf_name = scratch_folder / kwargs['outfile']
    print(outf_name)

    stdout = run_command(
        [
            PYTHON,
            'bin/get_function_members_fasta.py',
            '-c',
            kwargs['cluster_id'],
            '-o',
            outf_name
        ],
        print_output=False
    )

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')

    return kwargs['outfile']


@app.task(bind=True, name='python-backend.storeMappingResults')  
def submit_store_mapping_results(self, *args, **kwargs):
    
    # process taks
    print(f'New python backend submission to store mapping results: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['jobid']

    stdout = run_command(
        [
            PYTHON,
            'bin/post_process_results.py',
            '--jobid',
            kwargs['jobid'],
            '--fasta_in',
            kwargs['fasta_in'],
            '--result_in',
            kwargs['result_in']
        ],
        print_output=False
    )

    status = "ok"
    
    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')
        

    return stdout
