from backend.celery import broker_url, result_backend

# List of modules to import when the Celery worker starts.
imports = ('backend.tasks',)

# Worker settings.
worker_redirect_stdouts_level = 'INFO'
worker_prefetch_multiplier = 1
worker_concurrency = 1
